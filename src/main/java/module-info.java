module dev.hatsat {
	requires javafx.controls;
	requires javafx.fxml;
	requires java.logging;

	opens dev.hatsat to javafx.fxml;
	exports dev.hatsat;

	opens dev.hatsat.controllers to javafx.fxml;
	exports dev.hatsat.controllers;
	opens dev.hatsat.game to javafx.fxml;
	exports dev.hatsat.game;
	opens dev.hatsat.protocol to javafx.fxml;
	exports dev.hatsat.protocol;
}