package dev.hatsat.controllers;

import dev.hatsat.App;
import dev.hatsat.Data;
import dev.hatsat.game.Client;
import dev.hatsat.game.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Waiting implements Initializable {

	@FXML public TableView<User> playerTable;
	@FXML public TableColumn<User, Integer> No;
	@FXML public TableColumn<User, String> Username;
	@FXML public TableColumn<User, Boolean> Status;
	@FXML public Text usernameT = new Text();
	@FXML public TextArea playersTestArea = new TextArea();
	@FXML public Button startGameBtn = new Button();
	@FXML public ListView<String> playerList = new ListView<>();


	protected ArrayList<User> players = null;
	public static Client client;

	ObservableList<User> oyuncular;

	static String username   = "";
	static String ip         = "127.0.0.1";

	protected static int size       ;
	protected static int unusedArea ;
	protected static int pointToWin ;
	protected static int totalGame  ;
	protected static int port = 12345;

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {

		Waiting.username = Data.username;
		Waiting.size = Data.size;
		Waiting.unusedArea = Data.unusedArea;
		Waiting.pointToWin = Data.pointToWin;
		Waiting.totalGame = Data.totalGame;
		Waiting.port = Data.port;
		Waiting.ip = Data.ip;

		System.out.println(
				"username: " + Waiting.username + " " +
				"size: " + Waiting.size + " " +
				"unusedArea: " + Waiting.unusedArea + " " +
				"pointToWin: " + Waiting.pointToWin + " " +
				"totalGame: " + Waiting.totalGame + " " +
				"ip: " + Waiting.ip
		);

		playerList.getItems().add("Hello world 1");
		playerList.getItems().add("Hello world 2");
		playerList.getItems().add("Hello world 3");

		for (User player : Data.players) {
			playerList.getItems().removeAll();
			playerList.getItems().add(player.username);
		}

		this.startGameBtn.setVisible(Data.start_button);
		this.usernameT.setText(username);
		this.displayRules();
	}

	public void showStartGameBtn () {
		System.out.println("START BUTTON SHOW");
		this.startGameBtn.setVisible(true);
	}

	public ArrayList<User> getPlayers() {
		return this.players;
	}

	public void setPlayers(ArrayList<User> players) {
		for (User player : Data.players) {
			playerList.getItems().removeAll();
			playerList.getItems().add(player.username);
		}
		System.out.println("SET PLAYERS AFTER JOIN");
	}

	public void setGameSettings(String username, String size,String unusedArea, String pointToWin, String totalGame,
			String ip, int port) {
		Waiting.ip = ip;
		Waiting.username = username;

		Waiting.pointToWin = Integer.parseInt(pointToWin);
		Waiting.totalGame  = Integer.parseInt(totalGame);
		Waiting.unusedArea = Integer.parseInt(unusedArea);
		Waiting.size       = Integer.parseInt(size);
		Waiting.port       = port;

		System.out.println("IN THE WAITONS STATE");
		System.out.println(
				"username: " + Waiting.username + " " +
				"size: " + Waiting.size + " " +
				"unusedArea: " + Waiting.unusedArea + " " +
				"pointToWin: " + Waiting.pointToWin + " " +
				"totalGame: " + Waiting.totalGame + " " +
				"ip: " + Waiting.ip
		);

		this.displayRules();
	}

	public void displayRules() {
		String rules = "";
		rules += String.format("Size: %s\n",         Waiting.size);
		rules += String.format("Unused Area: %s\n",  Waiting.unusedArea);
		rules += String.format("Point to win: %s\n", Waiting.pointToWin);
		rules += String.format("Total Game: %s\n",   Waiting.totalGame);
		rules += String.format("Port %s\n",          Waiting.port);

		System.out.println("IN THE DISPLA RULES");

		playersTestArea.setText(rules);
	}

	@FXML
	private void switchToPrimary() throws IOException {
		App.setRoot("primary");
	}
}
