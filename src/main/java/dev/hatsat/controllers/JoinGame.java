package dev.hatsat.controllers;

import dev.hatsat.App;
import dev.hatsat.game.Client;
import dev.hatsat.protocol.KTP;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.nio.ByteBuffer;

public class JoinGame {

	Client client;

	@FXML Button joinGameBtn = new Button();
	@FXML Button goBackBtn = new Button();
	@FXML TextField IPField = new TextField();
	@FXML TextField PORTField = new TextField();

	@FXML
	public void joinGame() throws IOException {
		System.out.println("Joined Game");
		int PORT = Integer.parseInt(PORTField.getText());
		String IP = IPField.getText();

		KTP ktp = new KTP();
		String join_req = ktp.request("JOIN", "");
		System.out.println(join_req);

		client = new Client(IP, PORT);
		Waiting.client = client;

		Thread thread = new Thread(client);
		thread.start();

		ByteBuffer buffer = ByteBuffer.allocate(1024);
		buffer.put(join_req.getBytes());
		buffer.flip();

		while (buffer.hasRemaining()) {
			client.socketChannel.write(buffer);
		}
	}

	public void joinSuccess() throws IOException
	{
		System.out.println("[+] JOINING GAME SUCCESS");

		FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("views/waiting.fxml"));
		fxmlLoader.load();
		Waiting waiting = (Waiting) fxmlLoader.getController();

		App.setRoot("waiting");
	}

	@FXML
	public void goBack() throws IOException {
		App.setRoot("start");
	}
}
