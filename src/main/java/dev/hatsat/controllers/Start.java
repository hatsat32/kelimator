package dev.hatsat.controllers;

import dev.hatsat.App;
import javafx.fxml.FXML;
import java.io.IOException;
import javafx.scene.control.Button;

public class Start {

	@FXML
	public Button createGameBtn;

	@FXML
	public Button joinGameBtn;

	@FXML
	public void createGameBtn() throws IOException {
		App.setRoot("create_game");
	}

	@FXML
	public void joinGameBtn() throws IOException {
		App.setRoot("join_game");
	}
}
