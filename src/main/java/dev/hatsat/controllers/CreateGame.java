package dev.hatsat.controllers;

import dev.hatsat.App;
import dev.hatsat.Data;
import dev.hatsat.game.Server;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import java.io.IOException;

public class CreateGame {

	static Server server;

	@FXML Button createGameBtn = null;
	@FXML Button goBackBtn     = null;

	@FXML TextField usernameTF   = new TextField();
	@FXML TextField sizeTF       = new TextField();
	@FXML TextField unusedAreaTF = new TextField();
	@FXML TextField pointToWinTF = new TextField();
	@FXML TextField totalGameTF  = new TextField();
	@FXML TextField ipTF         = new TextField();
	@FXML TextField portTF       = new TextField();

	@FXML
	public void createGame() throws IOException {

		String username = usernameTF.getText();
		String size = sizeTF.getText();
		String unusedArea = unusedAreaTF.getText();
		String pointToWin = pointToWinTF.getText();
		String totalGame = totalGameTF.getText();
		String ip = ipTF.getText();
		String port = portTF.getText();

		Data.username = username;
		Data.size = Integer.parseInt(size);
		Data.pointToWin = Integer.parseInt(pointToWin);
		Data.totalGame = Integer.parseInt(totalGame);
		Data.ip = ip;
		Data.port = Integer.parseInt(port);

		server = new Server(port.isEmpty() ? 12345 : Integer.parseInt(port));

		Thread thread = new Thread(server);
		thread.start();

		App.setRoot("waiting");
	}

	@FXML
	public void goBack() throws IOException {
		App.setRoot("start");
	}

}
