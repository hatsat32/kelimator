package dev.hatsat;

import dev.hatsat.game.User;

import java.util.ArrayList;
import java.util.HashMap;

public class Data {

	public static String username;

	// rules
	public static int size = 20;
	public static int unusedArea = 10;
	public static int pointToWin = 200;
	public static int totalGame = 3;

	// IP and port
	public static String  ip = "127.0.0.1";
	public static int port = 12345;

	public static ArrayList<User> players = new ArrayList<>();

	public static HashMap<String, String> rules = new HashMap<>();

	public static boolean start_button = true;


 }
