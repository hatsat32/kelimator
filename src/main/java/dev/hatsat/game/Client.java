package dev.hatsat.game;

import dev.hatsat.App;
import dev.hatsat.controllers.JoinGame;
import dev.hatsat.protocol.KTP;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Client implements Runnable {

	static int PORT;
	static String IP;

	InetSocketAddress hostAddress = null;
	public SocketChannel socketChannel;

	public static InetAddress inetAddress;
	Selector selector;

	public Client(String ip, int port) throws IOException {
		IP = ip;
		PORT = port;

		System.out.println("Client... started");

		hostAddress = new InetSocketAddress(IP, PORT);
		this.selector = Selector.open();

		this.socketChannel = SocketChannel.open();
		this.socketChannel.connect(hostAddress);
		this.socketChannel.configureBlocking(false);
		this.socketChannel.register(selector, SelectionKey.OP_CONNECT | SelectionKey.OP_READ);
	}


	@Override
	public void run() {
		try
		{
			while (true)
			{
				while (true) {
					if (this.selector.select() > 0) {
						Boolean doneStatus = pReadySet(this.selector.selectedKeys());
						if (doneStatus) {
							break;
						}
					}
				}
				this.socketChannel.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public Boolean pReadySet(Set readySet) throws Exception {
		SelectionKey key = null;
		Iterator iterator = readySet.iterator();

		while (iterator.hasNext()) {
			key = (SelectionKey) iterator.next();
			iterator.remove();
		}

		assert key != null;
		if (key.isConnectable()) {
			Boolean connected = processConnect(key);
			if (!connected) {
				return true;
			}
		}

		if (key.isReadable())
			this.handleRead(key);

		return false;
	}

	private Boolean processConnect(SelectionKey key) {
		SocketChannel sc_local = (SocketChannel) key.channel();
		try {
			while (sc_local.isConnectionPending()) {
				sc_local.finishConnect();
			}
		} catch (IOException e) {
			key.cancel();
			return false;
		}
		return true;
	}

	private void handleRead(SelectionKey key) throws IOException {
		SocketChannel sc_local = (SocketChannel) key.channel();
		StringBuilder sb = new StringBuilder();

		ByteBuffer buffer = ByteBuffer.allocate(1024);

		int read;
		while( (read = sc_local.read(buffer)) > 0 ) {
			buffer.flip();
			byte[] bytes = new byte[buffer.limit()];
			buffer.get(bytes);
			sb.append(new String(bytes));
			buffer.clear();
		}

		String msg = sb.toString();
		this.handleMsg(msg);
	}

	private void handleMsg(String msg) {
		System.out.println(msg);

		FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("views/join_game.fxml"));

		try {
			fxmlLoader.load();
			JoinGame joinGame = (JoinGame) fxmlLoader.getController();
			joinGame.joinSuccess();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
