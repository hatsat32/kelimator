package dev.hatsat.game;

import dev.hatsat.App;
import dev.hatsat.Data;
import dev.hatsat.controllers.Waiting;
import dev.hatsat.protocol.KTP;
import javafx.fxml.FXMLLoader;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.*;
import java.nio.channels.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

public class Server implements Runnable{

	static ServerSocketChannel serverSocketChannel;
	static ServerSocket serverSocket;
	static Selector selector;
	protected static Logger log = Logger.getLogger(Server.class.getName());

	static int PORT;

	public Server(int port) {
		PORT = port;

		try
		{
			serverSocketChannel = ServerSocketChannel.open();
			serverSocketChannel.configureBlocking(false);
			serverSocket = serverSocketChannel.socket();

			serverSocket.bind(new InetSocketAddress(PORT));

			//Create a new Selector object for detecting input from channels...
			selector = Selector.open();

			//Register ServerSocketChannel with Selector
			//for receiving incoming connections...
			serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
			log.info("Server started");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}


	public void _run()
	{
		do
		{
			try
			{
				// Get number of events (new connection(s)
				// and/or data transmissions from existing
				// connection(s))...
				int numKeys = selector.select();

				if (numKeys > 0)
				{
					// Extract event(s) that have been triggered ...
					Set eventKeys = selector.selectedKeys();

					// Set up Iterator to cycle though set of events...
					Iterator keyCycler = eventKeys.iterator();

					while (keyCycler.hasNext())
					{
						SelectionKey key = (SelectionKey) keyCycler.next();

						// Retrieve set of ready ops for
						// this key (as a bit pattern)...
						int keyOps = key.readyOps();

						// New connection.
						if ((keyOps & SelectionKey.OP_ACCEPT) == SelectionKey.OP_ACCEPT)
						{
							acceptConnection(key);
							continue;
						}

						// Data from existing client.
						if ((keyOps & SelectionKey.OP_READ) == SelectionKey.OP_READ)
						{
							acceptData(key);
						}
					}
				}
			}
			catch(IOException ioEx)
			{
				ioEx.printStackTrace();
				System.exit(1);
			}
		} while (true);
	}

	@Override
	public void run()
	{
		try
		{
			Iterator<SelectionKey> iter;
			SelectionKey key;

			while(serverSocketChannel.isOpen()) {
				selector.select();
				iter = selector.selectedKeys().iterator();

				while(iter.hasNext()) {
					key = iter.next();
					iter.remove();

					if(key.isAcceptable())
						acceptConnection(key);

					if(key.isReadable())
						this.acceptData(key);
				}
			}
			serverSocketChannel.close();
		}
		catch(IOException ioEx)
		{
			ioEx.printStackTrace();
			System.exit(1);
		}
	}

	protected static void acceptConnection(SelectionKey key) throws IOException
	{
		//Accept incoming connection and add to list.
		SocketChannel socketChannel;
		Socket socket;

		socketChannel = serverSocketChannel.accept();
		socketChannel.configureBlocking(false);
		socket = socketChannel.socket();
		System.out.println("Connection on " + socket + ".");

		//Register SocketChannel for receiving data...
		socketChannel.register(selector, SelectionKey.OP_READ);

		//Avoid re-processing this event as though it
		//were a new one (next time through loop)...
		selector.selectedKeys().remove(key);
	}

	protected void acceptData(SelectionKey key) throws IOException
	{
		//Accept data from existing connection.
		SocketChannel socketChannel;
		Socket socket;
		ByteBuffer buffer = ByteBuffer.allocate(2048);
		//Above used for reading/writing data from/to SocketChannel.

		socketChannel = (SocketChannel) key.channel();
		buffer.clear();

		int numBytes = socketChannel.read(buffer);
		System.out.println(numBytes + " bytes read.");
		socket = socketChannel.socket();

		//OP_READ event also triggered by closure of
		//connection or error of some kind. In either
		//case, numBytes = -1.
		if (numBytes == -1)
		{
			//Request that registration of this key's
			//channel with its selector be cancelled...
			key.cancel();

			System.out.println("\nClosing socket " + socket + "...");
			closeSocket(socket);
		}
		else
		{
			try
			{
				/*
				Reset buffer pointer to start of buffer,
				prior to reading buffer's contents and
				writing them to the SocketChannel...
				*/
				buffer.flip();
				StringBuilder sb = new StringBuilder();
				byte[] bytes = new byte[buffer.limit()];
				buffer.get(bytes);
				sb.append(new String(bytes));
				buffer.clear();

				String res = this.handleRequest(sb.toString());

				socketChannel.write(ByteBuffer.wrap(res.getBytes()));

			}
			catch (IOException ioEx)
			{
				System.out.println("\nClosing socket " + socket + "...");
				closeSocket(socket);
			}
		}

		//Remove this event, to avoid re-processing it
		//as though it were a new one...
		selector.selectedKeys().remove(key);
	}

	protected static void closeSocket(Socket socket)
	{
		try
		{
			if (socket != null)
				socket.close();
		}
		catch (IOException ioEx)
		{
			System.out.println("*** Unable to close socket! ***");
		}
	}

	public static void closeServer()
	{
		try {
			serverSocketChannel.close();
			serverSocket.close();
		} catch (IOException e)
		{
			//
		}
	}

	public String handleRequest(String req)
	{
		String res;
		System.out.println(req);

		KTP ktp = new KTP();
		ktp.parseRequest(req);

		if (ktp.method.equals("JOIN")) {

			res = this.handleJoin(ktp);
		}
		else {
			res = "30 FAIL KTP/0.1";
		}

		return res;
	}

	public static void playersBroadcast(ArrayList<User> players) throws IOException {
		StringBuilder pls = new StringBuilder();

		for (User player : players) {
			pls.append(player.username).append(";");
		}
		pls.deleteCharAt(pls.length()-1);
		ByteBuffer msgBuf = ByteBuffer.wrap(pls.toString().getBytes());

		for (SelectionKey key: selector.keys()) {
			if (key.isValid() && key.channel() instanceof SocketChannel) {
				SocketChannel sc = (SocketChannel) key.channel();
				sc.write(msgBuf);
			}
		}
	}

	public String handleJoin(KTP ktp) {
		Data.players.add(new User(1, ktp.path, "", true));
		FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("views/waiting.fxml"));

		try {
			fxmlLoader.load();
			Waiting waiting = (Waiting) fxmlLoader.getController();
			waiting.setPlayers(Data.players);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ktp.response("20");
	}

}
