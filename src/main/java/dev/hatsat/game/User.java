package dev.hatsat.game;

public class User {
	public int no;
	public String username;
	public String secret;
	public boolean available;

	public User(int no, String username, String secret, boolean available) {
		this.no = no;
		this.username = username;
		this.secret = secret;
		this.available = available;
	}
}
