package dev.hatsat.protocol;

public enum Codes {

	// info
	NEWPLAYER (11, "11"),
	PLAYERS (12, "12"),

	// success
	SUCCESS (20, "20"),

	BAD (40, "40"),
	UNAUTHORIZED (43, "43"),

	ERROR (50, "50")
	;

	Codes(int i, String s) {

	}
}
