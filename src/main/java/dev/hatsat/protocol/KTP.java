package dev.hatsat.protocol;


/**
 * Kelimator Transfer Protocol (KTP)
 *
 * The next generation gaming protocol :)
 */
public class KTP {

	public final String version = "KTP/0.1";
	static final String trtd = "\n\r";
	Codes codes;
	Methods methods;

	public String request;
	public String response;

	public int status;
	public String method;
	public String path;

	public void parseRequest(String request) {
		String[] req = request.split("\n\r\n\r", 2);

		this.parseRequestHeader(req[0]);
		this.parseRequestBody(req[1]);
	}

	public void parseRequestHeader(String header) {
		String[] header_lines = header.split(trtd);

		String[] head = header_lines[0].split(" ", 3);
		this.method = head[0];
		this.path = head[0];
	}

	public void parseRequestBody(String body) {

	}

	public void parseResponse(String response) {
		this.response = response;


	}

	public void parseResponseHeader() {

	}

	public void parseResponseBody() {

	}

	public String request(String method, String body) {
		this.method = method;

		String req = String.format("%s %s %s %s",method, "aaa", this.version, trtd);
		req += String.format("%s%s", trtd, trtd);
		req += String.format("%s", body);

		return req;
	}

	public String response(String status)
	{
		String res = String.format("%s %s %s", status, Codes.SUCCESS, this.version);
		res += String.format("%s%s", trtd, trtd);
		res += String.format("%s", "body");

		return res;
	}
}
